//
//  ViewController.swift
//  AR Measure
//
//  Created by Cameron Gough on 15/11/2018.
//  Copyright © 2018 Cameron Gough. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {
    
    @IBOutlet var sceneView: ARSCNView!
    
    struct CGPoint{}
    
    // array for the position nodes
    var dotNodes = [SCNNode]()
    
    // the text that will display the distance
    var textNode = SCNNode()
    var textNode2 = SCNNode()
    var textNode3 = SCNNode()
    
    // the distance values
    var distanceArray: [Double] = []
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        sceneView.debugOptions = [SCNDebugOptions.showFeaturePoints]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //if more than 2 points selected remove points
        if dotNodes.count >= 3 {
            for dot in dotNodes {
                dot.removeFromParentNode()
            }
            dotNodes = [SCNNode]()
        }
        
        // add node at loction touched
        if let touchLocation = touches.first?.location(in: sceneView) {
            let hitTestResults = sceneView.hitTest(touchLocation, types: .featurePoint)
            
            if let hitResult = hitTestResults.first {
                addDot(at: hitResult)
            }
            
        }
    }
    
    // function that adds the nodes to the screen
    func addDot(at hitResult : ARHitTestResult) {
        let dotGeometry = SCNSphere(radius: 0.002)
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.yellow
        
        dotGeometry.materials = [material]
        
        let dotNode = SCNNode(geometry: dotGeometry)
        
        // sets dotnode position to the position tapped on the screen
        dotNode.position = SCNVector3(hitResult.worldTransform.columns.3.x, hitResult.worldTransform.columns.3.y, hitResult.worldTransform.columns.3.z)
        
        //adds the node to the sceneView
        sceneView.scene.rootNode.addChildNode(dotNode)
        
        //adds the dotnode to dotNodes array
        dotNodes.append(dotNode)
        
        // if there are two nodes calculate distance
        if dotNodes.count >= 2 {
            calculate()
            
        }
        
        if dotNodes.count >= 3 {
            
            calculate2()
            calculateArea()
        }
        
        
    }
    
    
    
    
    func calculate (){
        // let start position be the first node
        let start = dotNodes[0]
        
        // let end position be the second node
        let end = dotNodes[1]
        
       
        //these are for testing purposes in xcode terminal and how the location
        print(start.position)
        print(end.position)
    
        //finds hypotenuse between the two points  works the same as √(𝑚𝑥−𝑐𝑥)2+(𝑚𝑦−𝑐𝑦)2
        // pow means power off so thats where the squaring takes place
        let distance = sqrt(
            pow(end.position.x - start.position.x, 2) +
            pow(end.position.y - start.position.y, 2) +
            pow(end.position.z - start.position.z, 2)
        )
        //convert the distance to cm
        distanceArray.insert(Double(distance*100), at: 0)
        
        //make a string that saves the double to 2 decimal places
        let distanceString = String(format: "%.2f", distanceArray[0])
      
        //update text node
        updateText(text: "length 1: \(String(distanceString))cm", atPosition: start.position)
        
       
    }
    
    func calculate2 (){
        // let start position be the first node
        let start = dotNodes[1]
        
        // let end position be the second node
        let end = dotNodes[2]
        
        //these are for testing purposes in xcode terminal and how the location
        print(start.position)
        print(end.position)
        
        //finds hypotenuse between the two points  works the same as √(𝑚𝑥−𝑐𝑥)2+(𝑚𝑦−𝑐𝑦)2
        // pow means power off so thats where the squaring takes place
        let distance = sqrt(
            pow(end.position.x - start.position.x, 2) +
                pow(end.position.y - start.position.y, 2) +
                pow(end.position.z - start.position.z, 2)
        )
       
        //convert the distance to cm
        distanceArray.insert(Double(distance*100), at: 1)
        
        //make a string that saves the double to 2 decimal places
        let distanceString = String(format: "%.2f", distanceArray[1])
        
        //update text node
        updateText2(text: "Length 2: \(String(distanceString))cm", atPosition: start.position)
        
        
        
    }
    
    func calculateArea (){
        
        //define sides as lengths from the array of distances
        let side1 = distanceArray[0]
        let side2 = distanceArray[1]
        
        //set position for the text as the last node placed
        let position = dotNodes[2]
        
        //caculate area of both sides as a rectangle
        let area = side1 * side2
        
        // convert area to string with 2 decimal places
        let areaString = String(format: "%.2f", area)
        
        //update the text node
        updateText3(text: "Rectangle Area: \(String(areaString))cm2", atPosition: position.position)
        
    }
    
    func updateText(text: String, atPosition position: SCNVector3){
        
        //remove form hierarchy for more efficent rendering
        textNode.removeFromParentNode()
    
        // sets up the geometry of the text for 3d space
        let textGeometry = SCNText(string: text, extrusionDepth: 1.0)
        
        //sets colour to yellow
        textGeometry.firstMaterial?.diffuse.contents = UIColor.yellow
        
        //sets textnode geometry to text geometry
        textNode = SCNNode(geometry: textGeometry)
        
        //sets position of text
        textNode.position = SCNVector3(position.x, position.y + 0.01, position.z)
        
        //sets scale of text
        let fontSize = Float(0.001)
        textNode.scale = SCNVector3(fontSize, fontSize, fontSize)
        
        //adds text to the sceneView
        sceneView.scene.rootNode.addChildNode(textNode)
        
        
        
    }
    func updateText2(text: String, atPosition position: SCNVector3){
        
        //remove form hierarchy for more efficent rendering
        textNode2.removeFromParentNode()
        
        // sets up the geometry of the text for 3d space
        let textGeometry = SCNText(string: text, extrusionDepth: 1.0)
        
        //sets colour to yellow
        textGeometry.firstMaterial?.diffuse.contents = UIColor.yellow
        
        //sets textnode geometry to text geometry
        textNode2 = SCNNode(geometry: textGeometry)
        
        //sets position of text
        textNode2.position = SCNVector3(position.x, position.y + 0.01, position.z)
        
        //sets scale of text
        let fontSize = Float(0.001)
        textNode2.scale = SCNVector3(fontSize, fontSize, fontSize)
        
        //adds text to the sceneView
        sceneView.scene.rootNode.addChildNode(textNode2)
        
    }
    
    func updateText3(text: String, atPosition position: SCNVector3){
        
        //remove form hierarchy for more efficent rendering
        textNode3.removeFromParentNode()
        
        // sets up the geometry of the text for 3d space
        let textGeometry = SCNText(string: text, extrusionDepth: 1.0)
        
        //sets colour to yellow
        textGeometry.firstMaterial?.diffuse.contents = UIColor.yellow
        
        //sets textnode geometry to text geometry
        textNode3 = SCNNode(geometry: textGeometry)
        
        //sets position of text
        textNode3.position = SCNVector3(position.x, position.y + 0.01, position.z)
        
        //sets scale of text
        let fontSize = Float(0.001)
        textNode3.scale = SCNVector3(fontSize, fontSize, fontSize)
        
        //adds text to the sceneView
        sceneView.scene.rootNode.addChildNode(textNode3)
        
    }
    
    
    
    
}












